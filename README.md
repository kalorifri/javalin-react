# javalin-react



## Getting started

How to test:

Make sure java, maven and docker are installed
open terminal and run
```
docker-compose build
docker-compose up -d
start client-dev-tool/index.html
cd api
mvn package
java -jar target/api-1.jar
```
check if client-dev-tool/index.html is opened in browser, else open manually
add new box with form and click in button "New box"
get existing boxes by click on button "Get boxes"


A lot is left to be done, such as:
- All the frontend in React (+redux) (where css is written in less)
- Major need of cleanup and refactoring in Javalin app.
- Add validation in API when create box.
- Dockerise Java app to make testing easier for tester
- unit tests for java app
- unit tests for react app

The way data is structured for creating new box and fetching existing boxes is different on purpouse to somewhat simulate
a real world API where real world actions have been made based on given data.
