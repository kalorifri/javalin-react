
package api;

import io.javalin.Javalin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class App {
  public static void main(String[] args) {
    // Javalin app = Javalin.start(7000);

    Javalin app = Javalin.create(config -> {
        config.enableCorsForAllOrigins();
    }).start(7000);


    /**
     * Get all boxes as Json
     */
    app.get("/boxes", ctx -> {
      Boxes boxes = new Boxes();
      ctx.result(boxes.getAllJson());
    });

    /**
     * Create new box
     * Return new box id
     */
    app.post("/create-box", ctx -> {

      try {
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = (JSONObject) parser.parse(ctx.body());

        Box box = new Box(
          (String) jsonObj.get("name"),
          (String) jsonObj.get("country"),
          (String) jsonObj.get("color"),
          Float.parseFloat((String) jsonObj.get("weight"))
        );
        BoxHandler boxHandler = new BoxHandler(box);
        int id = boxHandler.save();

        // Return result
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        ctx.result(obj.toJSONString());
      } catch(Exception e) {
        // Error
        System.out.println("/create-box error: " + e.toString());

        // TODO: return error code and human friendly error message
        ctx.result("{}");
      }
    });

    app.get("/", ctx -> ctx.result("{}"));
  }
}
