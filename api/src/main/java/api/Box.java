package api;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

/**
 * Box class
 * Is immutable
 */
public class Box {

    private int id;
    private String name;
    private String country;
    private String color;
    private float weight;

    /**
     * Box without id
     * @param name Shipping name
     * @param country Shipping country
     * @param color Box color formatted as rgb
     * @param weight Box weight in KG
     */
    public Box(String name, String country, String color, float weight)
    {
        this.id = 0;
        this.name = name;
        this.country = country;
        this.color = color;
        this.weight = weight;
    }

    /**
     * Box with id
     * @param id Box id
     * @param name Shipping name
     * @param country Shipping country
     * @param color Box color formatted as rgb
     * @param weight Box weight in KG
     */
    public Box(int id, String name, String country, String color, float weight)
    {
        this.id = id;
        this.name = name;
        this.country = country;
        this.color = color;
        this.weight = weight;
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getColor()
    {
        return this.color;
    }

    public String getContry()
    {
        return this.country;
    }

    /**
     * Get shipping price in minor units (Cent when dollar)
     * Price is based on weight and country
     * @return int shipping price. Defaults to 0 when unavailable country
     */
    public int getPrice()
    {
        // Shipping cost to each country per kilo in SEK and in major units (Dollar when Dollar)
        Map<String, Float> map = new HashMap<String, Float>();
        map.put("Sweden", (float) 1.3);
        map.put("China", (float) 4);
        map.put("Brazil", (float) 8.6);
        map.put("Australia", (float) 7.2);

        if (map.containsKey(this.getContry())) {
            return Math.round(this.getWeight() * map.get(this.getContry()) * 100);
        }
        return 0;
    }

    /**
     * Return human friendly shipping price formatted in SEK
     * @return
     */
    public String getFormattedPrice()
    {
        String currencyCode = "SEK"; // ISO-4217 3 letter currency code. In this case always SEK (Swedish krona)

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        Currency currency = Currency.getInstance(currencyCode);
        format.setCurrency(currency);

        // Price presented in major units (Dollar when Dollar)
        return format.format((float) (this.getPrice() * 0.01)) + " " + currencyCode;
    }

    /**
     * Get Box weight in KG
     * @return float Box weight in KG
     */
    public float getWeight()
    {
        return this.weight;
    }
}
