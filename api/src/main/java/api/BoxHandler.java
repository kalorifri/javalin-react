package api;


/**
 * Create Update Delete Box
 */
public class BoxHandler {
    private Box box;

    public BoxHandler(Box box)
    {
        this.box = box;
    }

    /**
     * Save Box
     * @return int id of Created/Update box Defaults to 0 when error
     */
    public int save()
    {
        if (this.box.getId() > 0) {
            // Update
            // TODO: Add update box code
            return this.box.getId();
        } else {
            // Create
            try {
                MysqlQuery query = new MysqlQuery(
                        "insert into boxes (`shipping_address_name`, `shipping_address_country`, `color_rbg`, `weight`) values (?, ?, ?, ?)");
                query.prepareValue(this.box.getName());
                query.prepareValue(this.box.getContry());
                query.prepareValue(this.box.getColor());
                query.prepareValue(this.box.getWeight());
                return query.executeUpdate();
            } catch (Exception e) {
                // Silent fail
                return 0;
            }
        }
    }
}
