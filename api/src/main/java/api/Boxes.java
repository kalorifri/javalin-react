package api;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.ResultSet;

public class Boxes {

    public Boxes() {
        // Constructor, does nothing at the moment
    }

    /**
     * Get all boxes order by id desc and return as Json formatted string
     * @return
     */
    public String getAllJson() {
        String jsonResult = "{}";

        try {
            JSONObject obj;
            JSONObject shippingObj;
            JSONArray list = new JSONArray();

            MysqlQuery query = new MysqlQuery("SELECT * from boxes order by id desc;");
            ResultSet results = query.executeQuery();

            Box rowBox;

            if (results != null) {
                while (results.next()) {
                    rowBox = new Box(
                        results.getInt("id"),
                        results.getString("shipping_address_name"),
                        results.getString("shipping_address_country"),
                        results.getString("color_rbg"), // Note: Typo in db, should be rgb
                        results.getFloat("weight")
                    );

                    // Shipping
                    shippingObj = new JSONObject();
                    shippingObj.put("name", rowBox.getName());
                    shippingObj.put("country", rowBox.getContry());

                    obj = new JSONObject();
                    obj.put("id", rowBox.getId());
                    obj.put("shipping_address", shippingObj);
                    obj.put("weight", rowBox.getWeight());
                    obj.put("price", rowBox.getPrice());
                    obj.put("formatted_price", rowBox.getFormattedPrice());
                    obj.put("color_rgb", rowBox.getColor());

                    list.add(obj);
                }
            }

            jsonResult = list.toJSONString();

            results.getStatement().close();
        } catch(Exception e) {
            // Silent fail, behave as no result found
            System.out.println("Boxes getAllJson mysql failed: " + e.toString());
        }
        return jsonResult;
    }
}
