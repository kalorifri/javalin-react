package api;

import java.sql.Connection;
import java.sql.DriverManager;

/*
 * MySQL connection wraper, singleton pattern
 * How to use:
 * MysqlConnection db = MysqlConnection.getInstance();
 * MySQL Connection = db.getConnection();
 */
public class MysqlConnection {
    private static MysqlConnection single_instance = null;

    // private MySQL mysql;
    private Connection connection;

    private MysqlConnection() {
        try {
            // Connection connect;
            // PreparedStatement preparedStatement;

            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/jr?"
                            + "user=root&password=rootpassword");
        } catch (Exception e) {
            // Silent fail
        }
    }

    /**
     * Static method to create instance of Singleton class
     */
    public static MysqlConnection getInstance() {
        if (single_instance == null)
            single_instance = new MysqlConnection();

        return single_instance;
    }

    public Connection getConnection() {
        return this.connection;
    }
}
