package api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MysqlQuery {

    private int valueKey;
    private PreparedStatement preparedStatement;

    public MysqlQuery(String query)
    {
        try {
            MysqlConnection mysqlConnection = MysqlConnection.getInstance();
            Connection connection = mysqlConnection.getConnection();
            this.preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            this.valueKey = 1;
        } catch (Exception e) {
            // Silent fail
            System.out.println("error MysqlQuery");
        }
    }

    public void prepareValue(float value) {
        try {
            this.preparedStatement.setFloat(valueKey, value);
            valueKey++;
        } catch (Exception e) {
            // Silent fail
            System.out.println("error prepareValue float " + valueKey);
        }
    }

    public void prepareValue(String value) {
        try {
            this.preparedStatement.setString(valueKey, value);
            valueKey++;
        } catch (Exception e) {
            // Silent fail
            System.out.println("error prepareValue string " + valueKey);
        }
    }

    public ResultSet executeQuery()
    {
        try {
            return this.preparedStatement.executeQuery();
        }  catch (Exception e) {
            return null;
        }
    }

    /**
     * Exectute prepared MySQL query
     * @return int Auto generated id Defaults to 0
     */
    public int executeUpdate()
    {
        try {
            this.preparedStatement.executeUpdate();
            ResultSet rs = this.preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
                System.out.println("error executeUpdate");
        }
        return 0;
    }
}
