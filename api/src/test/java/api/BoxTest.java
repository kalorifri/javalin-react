package api;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BoxTest {
    @Test
    public void testNoId() {
        Box box = new Box("Name", "Country", "0,0,0", 1);
        assertEquals(0, box.getId());
    }

    @Test
    public void getValues() {
        Box box = new Box(2, "Name", "Sweden", "1,10,20", 1);
        assertEquals(2, box.getId());
        assertEquals("Name", box.getName());
        assertEquals("Sweden", box.getContry());
        assertEquals("1,10,20", box.getColor());
    }

    @Test
    public void testCalculatePrice() {
        Box box = new Box(1, "Name", "Sweden", "1,10,20", 2);
        assertEquals(260, box.getPrice()); // 1.3 * 2 * 100
        assertEquals("2,6 SEK", box.getFormattedPrice());

        Box box2 = new Box(1, "Name", "China", "1,10,20", 2);
        assertEquals(800, box2.getPrice()); // 4 * 2 * 100
        assertEquals("8 SEK", box2.getFormattedPrice());

        Box box3 = new Box(1, "Name", "Brazil", "1,10,20", 2);
        assertEquals(1720, box3.getPrice()); // 8.6 * 2 * 100
        assertEquals("17,2 SEK", box3.getFormattedPrice());

        Box box4 = new Box(1, "Name", "Australia", "1,10,20", 2);
        assertEquals(1440, box4.getPrice()); // 7.2 * 2 * 100
        assertEquals("14,4 SEK", box4.getFormattedPrice());
    }

    @Test
    public void testCalculatePriceUnavailableCountry() {
        Box box = new Box("Name", "Country", "0,0,0", 1);
        assertEquals(0, box.getPrice()); // 0 * 1
        assertEquals("0 SEK", box.getFormattedPrice());
    }
}
