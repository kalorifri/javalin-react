
CREATE TABLE IF NOT EXISTS `boxes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `weight` float NOT NULL,
  `color_rbg` varchar(100) NOT NULL,
  `shipping_address_country` varchar(100) NOT NULL,
  `shipping_address_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `boxes` (`id`, `weight`, `color_rbg`, `shipping_address_country`, `shipping_address_name`) VALUES
(1, 1.2, '255,255,255', 'sweden', 'John Doe'),
(2, 3.14, '255,255,255', 'Australia', 'Jane Doe');